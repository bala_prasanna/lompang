//
//  Admin.h
//  durians
//
//  Created by Zhongcai on 3/6/14.
//  Copyright (c) 2014 Zhongcai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Config: NSManagedObject

@property (nonatomic, retain) NSString *acct;
@property (nonatomic, retain) NSString *apns;
@property (nonatomic, retain) NSString *token;
@property (nonatomic, retain) NSString *user;
@property (nonatomic, retain) NSNumber *version;

@end