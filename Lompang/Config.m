//
//  Admin.m
//  durians
//
//  Created by Zhongcai on 3/6/14.
//  Copyright (c) 2014 Zhongcai. All rights reserved.
//

#import "Config.h"

@implementation Config

@dynamic acct;
@dynamic apns;
@dynamic token;
@dynamic user;
@dynamic version;

@end