//
//  Lompang.h
//  Lompang
//
//  Created by Zhongcai Ng on 20/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol LompangDelegate <NSObject>
@required

-(void)socketConnected:(NSDictionary *)dict;
// Delegate method when connected.
// Response dict will contain autogenerated 'user' identifier.

-(void)socketDisconnected;
// Delegate method when disconnected.

-(void)socketReceivedErr:(NSDictionary *)dict;
// Delegate method when encountered error.
// Response dict will contain details of error.

-(void)socketRegistered:(NSDictionary *)dict;
// Delegate method when registered for push notifications.
// Response dict will contain APNS token string.

-(void)socketUnregistered;
// Delegate method when unregistered for push notifications.

-(void)socketReceivedDict:(NSDictionary *)dict;
// Delegate method when received 'user' or 'post' packages.
// Timestamp on packages are UNIX timestamps.
// Timestamp= 0 indicates user is currently online.

@end

@interface Lompang: NSObject

@property (nonatomic, weak) id delegate;

-(void)connectToAccount:(NSString *)acct WithToken:(NSString *)token;
// Connect to LompangSDK backend with email and token.
// If do not have credentials, please follow instructions on our Github page (github.com/ngzhongcai/lompang) to obtain one.

-(void)disconnect;
// Disconnect from LompangSDK.

-(void)registerApns:(NSData *)deviceToken;
// Register for push notifications.
// Call this method when received deviceToken in didRegisterForRemoteNotificationsWithDeviceToken delegate method.

-(void)unregisterApns;
// Unregister for push notifications.

-(void)update:(NSMutableDictionary *)userload;
// Attach a custom payload onto User.

-(void)link:(NSString *)group;
// Link a User to a Group.
// Even when iOS device disconnects (ie, network connectivity or 'Home' button is pressed), this User-Group relationship still holds.
// As such, we are able to dispatch APNS notifications to Users who are disconnected, but are still linked to the Group.
// When iOS device reconnects, the User immediately receives all 'user' and 'post' packages residing in the linked Group.
// Subsequent changes happening within the Group will be pushed to the User automatically.
// Examples of such changes include 'Users disconnecting', 'Users updating', 'Users posting', 'Users joining' among others.
// By default a User is linked to a Group that has the same namesake as its 'user' identifier.
// But a User can be linked to multiple Groups.
// In the case of single user / multi-device scenario, many Users can be linked under a common denominator, ie, Facebook ID

-(void)unlink:(NSString *)group;
// Unlink a User from a Group.

-(void)join:(NSString *)group;
// Join a User to a Group.
// Similar to a 'link' (see above), except that when iOS device disconnects, User-Group relationship is no longer true

-(void)unjoin:(NSString *)group;
// Unjoin a User from a group.

-(void)listen:(NSString *)group;
// 'Silently' listens to a Group.
// Similar to a 'join', except that a User's presence or actions are not broadcasted to others in the Group

-(void)unlisten:(NSString *)group;
// Unlisten a User from a Group

-(void)post:(NSString *)pid Pidload:(NSMutableDictionary *)pidload To:(NSString *)group;
// Post a message with 'pid' as unique identifier and 'pidload' as custom payload to 'group'.
// Users in 'group' will receive the custom payload, 'pidload'.
// Very important that developer specifies a unique 'pid' identifier for the post.
// If duplicated pid, payload will be overwritten on top of the previous.
// Can use this method for peer or group chatting.

-(void)unpost:(NSString *)pid;
// Remove the post with unique 'pid' identifier.
// Once intended receipient has received the message, call 'unpost' to remove it from the backend.
// This is so that subsequent visitors to the group will not receive the message again.

-(void)notify:(NSString *)message Group:(NSString *)group;
// Sends out a Apple Push notification message with '1' as badge.
// Developer needs to manually amend applicationIconBadgeNumber to zero when all messages are read.

@end