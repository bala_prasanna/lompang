//
//  Task.h
//  Lompang
//
//  Created by Zhongcai on 17/5/14.
//  Copyright (c) 2014 Zhongcai. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Task: NSManagedObject

@property (nonatomic, retain) NSString *tid;
@property (nonatomic, retain) NSNumber *timestamp;
@property (nonatomic, retain) NSData *data;

@end