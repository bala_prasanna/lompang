//
//  Task.m
//  saas
//
//  Created by Zhongcai on 17/5/14.
//  Copyright (c) 2014 Zhongcai. All rights reserved.
//

#import "Task.h"

@implementation Task

@dynamic tid;
@dynamic timestamp;
@dynamic data;

@end