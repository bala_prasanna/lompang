//
//  AppDelegate.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Lompang.h"
#import "MainViewController.h"
#import "Database.h"

@class Lompang, MainViewController, Database;

@interface AppDelegate: UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Lompang *lompang;
@property (strong, nonatomic) Database *database;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) Profile *profile;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSString *quadtree;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGFloat thick;
@property (strong, nonatomic) UIColor *beigeDark;
@property (strong, nonatomic) UIColor *beige;
@property (strong, nonatomic) UIColor *gray;
@property (strong, nonatomic) UIColor *red;

-(NSDateFormatter *)dateFormatter;
-(NSString *)quadtreeEncodeWithPrecision:(int)precision Coordinate:(CLLocation *)loc;
-(CLLocation *)quadtreeDecode:(NSString *)encoded;
-(NSString *)neighbor:(NSString *)encoded North:(NSNumber *)north East:(NSNumber *)east;

@end