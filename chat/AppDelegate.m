//
//  AppDelegate.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.width= [[UIScreen mainScreen] bounds].size.width;
    self.height= [[UIScreen mainScreen] bounds].size.height;
    self.thick= [[UIScreen mainScreen] scale]== 2.0 ? 0.5 : 1.0;
    self.beigeDark= [UIColor colorWithRed:(235.0/255.0) green:(228.0/255.0) blue:(220.0/255.0) alpha:1.0];
    self.beige= [UIColor colorWithRed:(245.0/255.0) green:(241.0/255.0) blue:(237.0/255.0) alpha:1.0];
    self.gray= [UIColor colorWithRed:(190.0/255.0) green:(190.0/255.0) blue:(190.0/255.0) alpha:1.0];
    self.red= [UIColor colorWithRed:(191.0/255.0) green:(30.0/255.0) blue:(45.0/255.0) alpha:1.0];
    
    self.database= [[Database alloc] init];
    self.profile= [[self database] fetchProfile];
    self.locationManager= [[CLLocationManager alloc] init];
    self.locationManager.delegate= self;
    [self.locationManager requestWhenInUseAuthorization];
    self.locationManager.distanceFilter= kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy= kCLLocationAccuracyBest;
    
    self.window= [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor= [UIColor clearColor];
    self.mainViewController= [[MainViewController alloc] init];
    self.window.rootViewController= self.mainViewController;
    [self.window makeKeyAndVisible];
    
    self.lompang= [[Lompang alloc] init];
    self.lompang.delegate= self;
    [self.lompang connectToAccount:@"<YOUR_EMAIL>" WithToken:@"YOUR_TOKEN"];

    return YES;
}

-(void)socketConnected:(NSDictionary *)dict {
    NSLog(@"@@@ socketConnected: %@", dict);
    [self.locationManager startUpdatingLocation];
    UIUserNotificationSettings *settings= [UIUserNotificationSettings
                                           settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert) categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    NSMutableDictionary *dictProfile= [[NSMutableDictionary alloc] init];
    [dictProfile setObject:[dict objectForKey:@"user"] forKey:@"user"];
    if([dict objectForKey:@"userload"]) {
        NSMutableDictionary *userload= [dict objectForKey:@"userload"];
        if([userload objectForKey:@"name"]) { [dictProfile setObject:[userload objectForKey:@"name"] forKey:@"name"]; }
        if([userload objectForKey:@"status"]) { [dictProfile setObject:[userload objectForKey:@"status"] forKey:@"status"]; }
    }
    [self.database updateProfile:dictProfile];
}

-(void)socketDisconnected {
    NSLog(@"@@@ socketDisconnected");
    [self.locationManager stopUpdatingLocation];
    self.quadtree= nil;
}

-(void)socketReceivedErr:(NSDictionary *)dict {
    NSLog(@"@@@ socketReceivedErr: %@", dict);
}

-(void)socketRegistered:(NSDictionary *)dict {
    NSLog(@"@@@ socketRegistered: %@", dict);
}

-(void)socketUnregistered {
    NSLog(@"@@@ socketUnregistered");
}

-(void)socketReceivedDict:(NSDictionary *)dict {
    NSLog(@"@@@ socketReceivedDict: %@", dict);
    NSString *type= [dict objectForKey:@"type"];
    
    // USER
    if([type isEqualToString:@"user"]) {
        NSMutableDictionary *dictUser= [[NSMutableDictionary alloc] init];
        if([dict objectForKey:@"user"]) { [dictUser setObject:[dict objectForKey:@"user"] forKey:@"user"]; [dictUser setObject:[NSNumber numberWithBool:NO] forKey:@"isHidden"]; }
        if([dict objectForKey:@"timestamp"]) { [dictUser setObject:[NSNumber numberWithLongLong:[[dict objectForKey:@"timestamp"] longLongValue]] forKey:@"timestamp"]; }
        if([dict objectForKey:@"userload"]) {
            NSMutableDictionary *userload= [dict objectForKey:@"userload"];
            if([userload objectForKey:@"name"]) { [dictUser setObject:[userload objectForKey:@"name"] forKey:@"name"]; }
            if([userload objectForKey:@"status"]) { [dictUser setObject:[userload objectForKey:@"status"] forKey:@"status"]; }
        }
        [[dict objectForKey:@"user"] isEqualToString:self.profile.user] ? [self.database updateProfile:dictUser] : [self.database updateUser:dictUser];
        return;
    }
    
    // POST
    if([type isEqualToString:@"post"]) {
        NSMutableDictionary *dictMsg= [[NSMutableDictionary alloc] init];
        [dictMsg setObject:[dict objectForKey:@"user"] forKey:@"from"];
        [dictMsg setObject:[dict objectForKey:@"group"] forKey:@"to"];
        [dictMsg setObject:[[dict objectForKey:@"pidload"] objectForKey:@"msg"] forKey:@"msg"];
        [dictMsg setObject:[NSNumber numberWithLongLong:[[dict objectForKey:@"timestamp"] longLongValue]] forKey:@"timestamp"];
        [dictMsg setObject:[dict objectForKey:@"pid"] forKey:@"pid"];
        [self.database updateMsg:dictMsg];
        [self.lompang unpost:[dict objectForKey:@"pid"]];
        
        NSMutableDictionary *dictUser= [[NSMutableDictionary alloc] init];
        [dictUser setObject:[dict objectForKey:@"user"] forKey:@"user"];
        [dictUser setObject:[NSNumber numberWithBool:YES] forKey:@"gotUnread"];
        [self.database updateUser:dictUser];
    }
}

-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [self.lompang registerApns:deviceToken];
}

-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"@@@ APNS ERROR: %@", error);
}

// CORE LOCATION
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSString *qt= [self quadtreeEncodeWithPrecision:10 Coordinate:[locations lastObject]];
    if([qt isEqualToString:self.quadtree]) { return; }
    [self.database hideAllUsers];
    self.quadtree= qt;
    [self.lompang join:self.quadtree];
}

// HELPER METHODS
-(NSDateFormatter *)dateFormatter {
    static NSDateFormatter *dateFormatter; static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter= [[NSDateFormatter alloc] init];
        dateFormatter.timeZone= [NSTimeZone defaultTimeZone];
        dateFormatter.dateFormat= @"d MMM yy',' h:mm a"; });
    return dateFormatter;
}

-(NSString *)quadtreeEncodeWithPrecision:(int)precision Coordinate:(CLLocation *)loc {
    CGFloat originLat= 0.0; CGFloat originLng= 0.0; CGFloat rangeLat= 90.0; CGFloat rangeLng= 180.0;
    NSMutableString *result= [[NSMutableString alloc] init];
    while(precision> 0) {
        rangeLat= rangeLat / 2.0; rangeLng= rangeLng / 2.0;
        if((loc.coordinate.longitude< originLng) && (loc.coordinate.latitude>= originLat)) {
            originLat= originLat+ rangeLat; originLng= originLng- rangeLng; [result appendString:@"0"]; }
        else if((loc.coordinate.longitude>= originLng) && (loc.coordinate.latitude>= originLat)) {
            originLat= originLat+ rangeLat; originLng= originLng+ rangeLng; [result appendString:@"1"]; }
        else if((loc.coordinate.longitude< originLng) && (loc.coordinate.latitude< originLat)) {
			originLat= originLat- rangeLat; originLng= originLng- rangeLng; [result appendString:@"2"]; }
        else { originLat= originLat- rangeLat; originLng= originLng+ rangeLng; [result appendString:@"3"]; }
		--precision; }
	return result;
}

-(CLLocation *)quadtreeDecode:(NSString *)encoded {
    CGFloat originLat= 0.0; CGFloat originLng= 0.0; CGFloat errorLat= 90.0; CGFloat errorLng= 180.0;
    int precision= (int)encoded.length; int currentPrecision= 0;
    while(currentPrecision< precision) {
        errorLng= errorLng / 2.0; errorLat= errorLat / 2.0; unichar quadrant= [encoded characterAtIndex:currentPrecision];
        if(quadrant== '0') { originLng= originLng- errorLng; originLat= originLat+ errorLat; }
        else if(quadrant== '1') { originLng= originLng+ errorLng; originLat= originLat+ errorLat; }
        else if(quadrant== '2') { originLng= originLng- errorLng; originLat= originLat- errorLat; }
        else { originLng= originLng+ errorLng; originLat= originLat- errorLat; }
        ++currentPrecision; }
    CLLocation *origin= [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(originLat, originLng) altitude:0.0 horizontalAccuracy:errorLat verticalAccuracy:errorLng timestamp:nil];
    return origin;
}

-(NSString *)neighbor:(NSString *)encoded North:(NSNumber *)north East:(NSNumber *)east {
    CLLocation *decoded= [self quadtreeDecode:encoded]; int precision= (int)encoded.length;
    CGFloat neighborLat= decoded.coordinate.latitude + decoded.horizontalAccuracy * [north intValue] * 2.0;
    CGFloat neighborLng= decoded.coordinate.longitude + decoded.verticalAccuracy * [east intValue] * 2.0;
    CLLocation *neighbor= [[CLLocation alloc] initWithLatitude:neighborLat longitude:neighborLng];
    NSString *encode= [self quadtreeEncodeWithPrecision:precision Coordinate:neighbor];
    return encode;
}

-(void)setBadge {
    NSManagedObjectContext *context= [self.database managedObjectContext];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSPredicate *predicate= [NSPredicate predicateWithFormat:@"gotUnread== %@", [NSNumber numberWithBool:YES]];
    request.entity= entity; request.predicate= predicate; NSUInteger count= [context countForFetchRequest:request error:nil];
    [UIApplication sharedApplication].applicationIconBadgeNumber= count> 0 ? 1 : 0;
}

-(void)applicationWillResignActive:(UIApplication *)application { }
-(void)applicationDidEnterBackground:(UIApplication *)application { [self setBadge]; }
-(void)applicationWillEnterForeground:(UIApplication *)application { }
-(void)applicationDidBecomeActive:(UIApplication *)application { }
-(void)applicationWillTerminate:(UIApplication *)application { [self setBadge]; }

@end