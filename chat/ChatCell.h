//
//  ChatCell.h
//  chat
//
//  Created by Zhongcai Ng on 20/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ChatCell: UITableViewCell {
    CGFloat width;
    CGFloat thick;
}

@property (strong, nonatomic) UILabel *other;
@property (strong, nonatomic) UILabel *msg;
@property (strong, nonatomic) UILabel *timestamp;

@end