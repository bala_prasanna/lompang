//
//  ChatCell.m
//  chat
//
//  Created by Zhongcai Ng on 20/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "ChatCell.h"

@implementation ChatCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        width= [[UIScreen mainScreen] applicationFrame].size.width;
        thick= [[UIScreen mainScreen] scale]== 2.0 ? 0.5 : 1.0;
        self.selectionStyle= UITableViewCellSelectionStyleNone;
        self.backgroundColor= [self appDelegate].beige;
        
        self.other= [[UILabel alloc] init];
        self.other.backgroundColor= [UIColor clearColor];
        self.other.textColor= [UIColor grayColor];
        self.other.font= [UIFont boldSystemFontOfSize:16.0];
        self.other.numberOfLines= 0;
        [self.contentView addSubview:self.other];
        
        self.msg= [[UILabel alloc] init];
        self.msg.backgroundColor= [UIColor clearColor];
        self.msg.textColor= [UIColor grayColor];
        self.msg.font= [UIFont systemFontOfSize:14.0];
        self.msg.numberOfLines= 0;
        [self.contentView addSubview:self.msg];
        
        self.timestamp= [[UILabel alloc] init];
        self.timestamp.backgroundColor= [UIColor clearColor];
        self.timestamp.textColor= [UIColor grayColor];
        self.timestamp.font= [UIFont italicSystemFontOfSize:10.0];
        self.timestamp.numberOfLines= 0;
        [self.contentView addSubview:self.timestamp];
    }
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated { [super setSelected:selected animated:animated]; }
-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }

@end