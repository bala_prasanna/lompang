//
//  ChatViewController.h
//  chat
//
//  Created by Zhongcai Ng on 20/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "User.h"
#import "ChatCell.h"

@interface ChatViewController: UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UITextFieldDelegate> {
    CGFloat width;
    CGFloat height;
    CGFloat thick;
    UIView *bottom;
    UITextField *field;
    UIButton *sendBtn;
    UITableView *chatTableView;
    NSDictionary *attributeNormal;
    NSDictionary *attributeItalics;
    NSDictionary *attributeBold;
    NSFetchedResultsController *chatFetchedResultsController;
}

@property (strong, nonatomic) User *other;

@end