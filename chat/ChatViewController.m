//
//  ChatViewController.m
//  chat
//
//  Created by Zhongcai Ng on 20/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController()
@end

@implementation ChatViewController

-(id)init {
    self= [super init];
    if(self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
        attributeNormal= [NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:14.0] forKey:NSFontAttributeName];
        attributeItalics= [NSDictionary dictionaryWithObject:[UIFont italicSystemFontOfSize:10.0] forKey:NSFontAttributeName];
        attributeBold= [NSDictionary dictionaryWithObject:[UIFont boldSystemFontOfSize:16.0] forKey:NSFontAttributeName];
    }
    return self;
}

-(void)loadView {
    width= [self appDelegate].width;
    height= [self appDelegate].height;
    thick= [self appDelegate].thick;
    self.view= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, height)];
    self.view.backgroundColor= [self appDelegate].beige;
    
    UIView *status= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 20.0)];
    status.backgroundColor= [UIColor blackColor];
    [self.view addSubview:status];
    
    UIView *top= [[UIView alloc] initWithFrame:CGRectMake(0.0, 20.0, width, 50.0)];
    top.backgroundColor= [self appDelegate].red;
    [self.view addSubview:top];
    
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(0.0, 26.0, width, 36.0)];
    title.font= [UIFont fontWithName:@"ChalkboardSE-Bold" size:22.0];
    title.backgroundColor= [UIColor clearColor];
    title.textAlignment= NSTextAlignmentCenter;
    title.textColor= [self appDelegate].beige;
    title.text= (self.other.name== nil || [self.other.name isEqualToString: @""]) ? @"Anonymous" : self.other.name;
    [self.view addSubview:title];
    
    UIButton *backBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn addTarget:self action:@selector(fireBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame= CGRectMake(0.0, 20.0, 50.0, 50.0);
    [backBtn setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [self.view addSubview:backBtn];
    
    bottom= [[UIView alloc] initWithFrame:CGRectMake(0.0, height -50.0, width, 50.0)];
    bottom.userInteractionEnabled= YES;
    bottom.backgroundColor= [self appDelegate].beigeDark;
    bottom.layer.borderColor= [UIColor grayColor].CGColor;
    bottom.layer.borderWidth= thick;
    
    UIView *text= [[UIView alloc] initWithFrame:CGRectMake(5.0, 5.0, width -75.0, 40.0)];
    text.backgroundColor= [self appDelegate].beige;
    text.layer.borderColor= [UIColor grayColor].CGColor;
    text.layer.borderWidth= thick;
    [bottom addSubview:text];
    
    UIImageView *circle= [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 0.0, 20.0, 40.0)];
    circle.image= [UIImage imageNamed:@"Circle"];
    [text addSubview:circle];
    
    UIImageView *cross= [[UIImageView alloc] initWithFrame:CGRectMake(width -110.0, 0.0, 20.0, 40.0)];
    cross.image= [UIImage imageNamed:@"Cross"];
    cross.userInteractionEnabled= YES;
    [text addSubview:cross];
    
    field= [[UITextField alloc] initWithFrame:CGRectMake(40.0, 0.0, width -150.0, 40.0)];
    field.contentVerticalAlignment= UIControlContentVerticalAlignmentCenter;
    field.autocorrectionType= UITextAutocorrectionTypeYes;
    field.font= [UIFont systemFontOfSize:22.0];
    field.returnKeyType= UIReturnKeySend;
    field.delegate= self;
    [text addSubview:field];
    
    sendBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [sendBtn addTarget:self action:@selector(fireSend) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setTitle:@"Send" forState:UIControlStateNormal];
    [sendBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    sendBtn.titleLabel.font= [UIFont fontWithName:@"AvenirNextCondensed-DemiBold" size:20.0];
    sendBtn.frame= CGRectMake(width -60.0, 0.0, 50.0, 50.0);
    [bottom addSubview:sendBtn];
    [self.view addSubview:bottom];
    
    chatTableView= [[UITableView alloc] initWithFrame:CGRectMake(0.0, 70.0, width, height -120.0) style:UITableViewStylePlain];
    chatTableView.backgroundColor= [self appDelegate].beige;
    chatTableView.delegate= self;
    chatTableView.dataSource= self;
    chatTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    [self.view addSubview:chatTableView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSMutableDictionary *dictUser= [[NSMutableDictionary alloc] init];
    [dictUser setObject:self.other.user forKey:@"user"];
    [dictUser setObject:[NSNumber numberWithBool:NO] forKey:@"gotUnread"];
    [[self appDelegate].database updateUser:dictUser];
    chatFetchedResultsController= [self chatFetchedResultsController];
}

-(NSFetchedResultsController *)chatFetchedResultsController {
    NSManagedObjectContext *context= [[self appDelegate].database managedObjectContext];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"Msg" inManagedObjectContext:context];
    NSPredicate *predicate= [NSPredicate predicateWithFormat:@"from== %@ || to== %@", self.other.user, self.other.user];
    NSSortDescriptor *sort= [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:YES];
    NSArray *sortDescriptors= [NSArray arrayWithObjects:sort, nil];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    request.entity= entity; request.predicate= predicate; request.sortDescriptors= sortDescriptors;
    chatFetchedResultsController= [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                    managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    chatFetchedResultsController.delegate= self; [chatFetchedResultsController performFetch:nil];
    return chatFetchedResultsController;
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [chatTableView reloadData];
    [self scrollToBottom];
    double delayInSeconds= 0.2; dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        NSMutableDictionary *dictUser= [[NSMutableDictionary alloc] init];
        [dictUser setObject:self.other.user forKey:@"user"];
        [dictUser setObject:[NSNumber numberWithBool:NO] forKey:@"gotUnread"];
        [[self appDelegate].database updateUser:dictUser];
    });
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return chatFetchedResultsController.fetchedObjects.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ChatCellIdentifier= @"ChatCellIdentifier";
    ChatCell *chatCell= [tableView dequeueReusableCellWithIdentifier:ChatCellIdentifier];
    if(chatCell== nil) { chatCell= [[ChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ChatCellIdentifier]; }
    Msg *msg= [chatFetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    chatCell.msg.text= msg.msg;
    NSDateFormatter *dateFormatter= [[self appDelegate] dateFormatter];
    chatCell.timestamp.text= [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[msg.timestamp longLongValue]/1000]];
    
    CGFloat msgBodyHeight= [msg.msg boundingRectWithSize:CGSizeMake(width - 20.0, INFINITY) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeNormal context:nil].size.height;
    NSString *timestamp= [[[self appDelegate] dateFormatter] stringFromDate:[NSDate dateWithTimeIntervalSince1970:[msg.timestamp longLongValue]/1000]];
    CGFloat timestampBodyHeight= [timestamp boundingRectWithSize:CGSizeMake(width - 20.0, INFINITY) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeItalics context:nil].size.height;
    
    if([msg.from isEqualToString:[self appDelegate].profile.user]) {
        chatCell.other.text= @"";
        chatCell.other.frame= CGRectZero;
        chatCell.msg.frame= CGRectMake(10.0, 8.0, width - 20.0, msgBodyHeight);
        chatCell.msg.textAlignment= NSTextAlignmentRight;
        chatCell.timestamp.frame= CGRectMake(10.0, 8.0 + msgBodyHeight + 8.0, width - 20.0, timestampBodyHeight);
        chatCell.timestamp.textAlignment= NSTextAlignmentRight;
    }
    else {
        chatCell.other.text= (self.other.name== nil || [self.other.name isEqualToString: @""]) ? @"Anonymous" : self.other.name;
        chatCell.other.frame= CGRectMake(10.0, 8.0, width - 20.0, 18.0);
        chatCell.msg.frame= CGRectMake(10.0, 8.0 + 18.0, width - 20.0, msgBodyHeight);
        chatCell.msg.textAlignment= NSTextAlignmentLeft;
        chatCell.timestamp.frame= CGRectMake(10.0, 8.0 + 18.0 + msgBodyHeight + 8.0, width - 20.0, timestampBodyHeight);
        chatCell.timestamp.textAlignment= NSTextAlignmentLeft;
    }
    
    return chatCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Msg *msg= [chatFetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    CGFloat msgHeight= [self returnMsgHeight:msg];
    return [msg.from isEqualToString:[self appDelegate].profile.user] ? msgHeight + 16.0 : msgHeight + 16.0 + 18.0;
}

-(CGFloat)returnMsgHeight:(Msg *)msg {
    CGFloat msgBodyHeight= [msg.msg boundingRectWithSize:CGSizeMake(width - 20.0, INFINITY) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeNormal context:nil].size.height;
    NSString *timestamp= [[[self appDelegate] dateFormatter] stringFromDate:[NSDate dateWithTimeIntervalSince1970:[msg.timestamp longLongValue]/1000]];
    CGFloat timestampBodyHeight= [timestamp boundingRectWithSize:CGSizeMake(width - 20.0, INFINITY) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeItalics context:nil].size.height;
    return msgBodyHeight + timestampBodyHeight;
}

-(void)fireBack {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideChatView" object:nil];
}

-(void)fireSend {
    if(field.text.length== 0) { return; }
    NSString *msg= [field.text copy]; field.text= @"";
    NSNumber *timestamp= [NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970] * 1000];
    NSString *pid= [NSString stringWithFormat:@"%@-%@", [self appDelegate].profile.user, timestamp];
    Profile *profile= [self appDelegate].profile;
    NSString *name= (profile.name== nil || [profile.name isEqualToString: @""]) ? @"Anonymous" : profile.name;
    
    NSMutableDictionary *dictMsg= [[NSMutableDictionary alloc] init];
    [dictMsg setObject:[self appDelegate].profile.user forKey:@"from"];
    [dictMsg setObject:msg forKey:@"msg"];
    [dictMsg setObject:pid forKey:@"pid"];
    [dictMsg setObject:timestamp forKey:@"timestamp"];
    [dictMsg setObject:self.other.user forKey:@"to"];
    [[self appDelegate].database updateMsg:dictMsg];
    
    NSMutableDictionary *pidload= [[NSMutableDictionary alloc] init];
    [pidload setObject:msg forKey:@"msg"];
    [[self appDelegate].lompang post:pid Pidload:pidload To:self.other.user];
    [[self appDelegate].lompang notify:[NSString stringWithFormat:@"%@: %@", name, msg] Group:self.other.user];
}

-(void)keyboardWillChangeFrame:(NSNotification *)notification {
    CGRect keyboardFrame= [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    float keyboardHeight= height -keyboardFrame.origin.y;
    double duration= [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        chatTableView.frame= CGRectMake(0.0, 70.0, width, height- 70.0 -49.0 -keyboardHeight);
        bottom.frame= CGRectMake(0.0, height -50.0 -keyboardHeight, width, 50.0);
        [self scrollToBottom];
    }];
}

-(void)scrollToBottom {
    if(chatTableView.contentSize.height< chatTableView.frame.size.height) { return; }
    double delayInSeconds= 0.1; dispatch_time_t popTime= dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
        CGPoint offset= CGPointMake(0, chatTableView.contentSize.height- chatTableView.frame.size.height);
        [chatTableView setContentOffset:offset animated:YES];
    });
}

-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
-(void)viewDidLoad { [super viewDidLoad]; }
-(void)didReceiveMemoryWarning { [super didReceiveMemoryWarning]; }

@end