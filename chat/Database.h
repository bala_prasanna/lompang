//
//  Database.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Msg.h"
#import "Profile.h"
#import "User.h"

@interface Database: NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

-(Msg *)fetchMsg:(NSString *)pid;
-(BOOL)updateMsg:(NSMutableDictionary *)dict;
-(Profile *)fetchProfile;
-(BOOL)updateProfile:(NSMutableDictionary *)dict;
-(User *)fetchUser:(NSString *)user;
-(BOOL)updateUser:(NSMutableDictionary *)dict;
-(BOOL)hideAllUsers;

@end