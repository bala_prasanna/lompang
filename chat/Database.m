//
//  Database.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "Database.h"

@implementation Database

@synthesize managedObjectContext= _managedObjectContext;
@synthesize managedObjectModel= _managedObjectModel;
@synthesize persistentStoreCoordinator= _persistentStoreCoordinator;

-(id)init {
    self= [super init];
    if(self) {
    }
    return self;
}

// MSG
-(Msg *)fetchMsg:(NSString *)pid {
    NSManagedObjectContext *context= [self managedObjectContext];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"Msg" inManagedObjectContext:context];
    NSPredicate *predicate= [NSPredicate predicateWithFormat:@"pid== %@", pid];
    request.entity= entity; request.predicate= predicate;
    NSArray *msgs= [context executeFetchRequest:request error:nil];
    return msgs.count> 0 ? [msgs objectAtIndex:0] : nil;
}

-(BOOL)updateMsg:(NSMutableDictionary *)dict {
    Msg *msg= [self fetchMsg:[dict objectForKey:@"pid"]];
    NSManagedObjectContext *context= [self managedObjectContext];
    if(!msg) { msg= [NSEntityDescription insertNewObjectForEntityForName:@"Msg" inManagedObjectContext:context]; }
    if([dict objectForKey:@"from"]) { msg.from= [dict objectForKey:@"from"]; }
    if([dict objectForKey:@"msg"]) { msg.msg= [dict objectForKey:@"msg"]; }
    if([dict objectForKey:@"pid"]) { msg.pid= [dict objectForKey:@"pid"]; }
    if([dict objectForKey:@"timestamp"]) { msg.timestamp= [dict objectForKey:@"timestamp"]; }
    if([dict objectForKey:@"to"]) { msg.to= [dict objectForKey:@"to"]; }
    return [context save:nil];
}

// PROFILE
-(Profile *)fetchProfile {
    NSManagedObjectContext *context= [self managedObjectContext];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"Profile" inManagedObjectContext:context];
    request.entity= entity; NSArray *me= [context executeFetchRequest:request error:nil];
    return me.count> 0 ? [me objectAtIndex:0] : [NSEntityDescription insertNewObjectForEntityForName:@"Profile" inManagedObjectContext:context];
}

-(BOOL)updateProfile:(NSMutableDictionary *)dict {
    Profile *profile= [self fetchProfile];
    NSManagedObjectContext *context= [self managedObjectContext];
    if([dict objectForKey:@"name"]) { profile.name= [dict objectForKey:@"name"]; }
    if([dict objectForKey:@"status"]) { profile.status= [dict objectForKey:@"status"]; }
    if([dict objectForKey:@"user"]) { profile.user= [dict objectForKey:@"user"]; }
    return [context save:nil];
}

// USER
-(User *)fetchUser:(NSString *)user {
    NSManagedObjectContext *context= [self managedObjectContext];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSPredicate *predicate= [NSPredicate predicateWithFormat:@"user== %@", user];
    request.entity= entity; request.predicate= predicate;
    NSArray *users= [context executeFetchRequest:request error:nil];
    return users.count> 0 ? [users objectAtIndex:0] : nil;
}

-(BOOL)updateUser:(NSMutableDictionary *)dict {
    User *user= [self fetchUser:[dict objectForKey:@"user"]];
    NSManagedObjectContext *context= [self managedObjectContext];
    if(!user) { user= [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context]; }
    if([dict objectForKey:@"gotUnread"]) { user.gotUnread= [dict objectForKey:@"gotUnread"]; }
    if([dict objectForKey:@"isHidden"]) { user.isHidden= [dict objectForKey:@"isHidden"]; }
    if([dict objectForKey:@"name"]) { user.name= [dict objectForKey:@"name"]; }
    if([dict objectForKey:@"status"]) { user.status= [dict objectForKey:@"status"]; }
    if([dict objectForKey:@"timestamp"]) { user.timestamp= [dict objectForKey:@"timestamp"]; }
    if([dict objectForKey:@"user"]) { user.user= [dict objectForKey:@"user"]; }
    return [context save:nil];
}

-(BOOL)hideAllUsers {
    NSManagedObjectContext *context= [self managedObjectContext];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    request.entity= entity; NSArray *users= [context executeFetchRequest:request error:nil];
    for(User *user in users) { user.isHidden= [NSNumber numberWithInt:1]; }
    return [context save:nil];
}

// CORE DATA
-(NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(NSManagedObjectContext *)managedObjectContext {
    if(_managedObjectContext!= nil) { return _managedObjectContext; }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if(coordinator!= nil) {
        _managedObjectContext= [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

-(NSManagedObjectModel *)managedObjectModel {
    if(_managedObjectModel!= nil) { return _managedObjectModel; }
    NSURL *modelURL= [[NSBundle mainBundle] URLForResource:@"chat" withExtension:@"momd"];
    _managedObjectModel= [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

-(NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if(_persistentStoreCoordinator != nil) { return _persistentStoreCoordinator; }
    NSURL *storeURL= [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"chat.sqlite"];
    NSError *error= nil;
    _persistentStoreCoordinator= [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options= [NSDictionary dictionaryWithObjectsAndKeys:
                            [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                            [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"@@@ COREDATA ERROR: %@", error.localizedDescription); abort();
    }
    return _persistentStoreCoordinator;
}

@end