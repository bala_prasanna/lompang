//
//  MainViewController.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ProfileViewController.h"
#import "ChatViewController.h"
#import "TitleCell.h"
#import "UserCell.h"

@class ProfileViewController, ChatViewController;

@interface MainViewController: UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate> {
    CGFloat width;
    CGFloat height;
    CGFloat thick;
    UITableView *mainTableView;
    NSFetchedResultsController *usersFetchedResultsController;
}

@property (strong, nonatomic) ProfileViewController *profileViewController;
@property (strong, nonatomic) ChatViewController *chatViewController;

@end