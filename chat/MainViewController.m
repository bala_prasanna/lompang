//
//  MainViewController.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController()
@end

@implementation MainViewController

-(id)init {
    self= [super init];
    if(self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showProfileView:) name:@"showProfileView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideProfileView:) name:@"hideProfileView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showChatView:) name:@"showChatView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideChatView:) name:@"hideChatView" object:nil];
        usersFetchedResultsController= [self usersFetchedResultsController];
    }
    return self;
}

-(void)loadView {
    width= [self appDelegate].width;
    height= [self appDelegate].height;
    thick= [self appDelegate].thick;
    self.view= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, height)];
    self.view.backgroundColor= [self appDelegate].beige;
    
    UIView *status= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 20.0)];
    status.backgroundColor= [UIColor blackColor];
    [self.view addSubview:status];
    
    UIView *top= [[UIView alloc] initWithFrame:CGRectMake(0.0, 20.0, width, 50.0)];
    top.backgroundColor= [self appDelegate].red;
    [self.view addSubview:top];
    
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(0.0, 26.0, width, 36.0)];
    title.font= [UIFont fontWithName:@"ChalkboardSE-Bold" size:22.0];
    title.backgroundColor= [UIColor clearColor];
    title.textAlignment= NSTextAlignmentCenter;
    title.textColor= [self appDelegate].beige;
    title.text= @"LompangChat";
    [self.view addSubview:title];
    
    UIButton *profileBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [profileBtn addTarget:self action:@selector(fireProfile) forControlEvents:UIControlEventTouchUpInside];
    profileBtn.frame= CGRectMake(0.0, 20.0, 50.0, 50.0);
    [profileBtn setImage:[UIImage imageNamed:@"Profile"] forState:UIControlStateNormal];
    [self.view addSubview:profileBtn];
    
    mainTableView= [[UITableView alloc] initWithFrame:CGRectMake(0.0, 70.0, width, height -70.0) style:UITableViewStylePlain];
    mainTableView.backgroundColor= [self appDelegate].beige;
    mainTableView.delegate= self;
    mainTableView.dataSource= self;
    mainTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    [self.view addSubview:mainTableView];
}

-(void)fireProfile { [[NSNotificationCenter defaultCenter] postNotificationName:@"showProfileView" object:nil]; }

-(void)showProfileView:(NSNotification *)notification {
    self.profileViewController= [[ProfileViewController alloc] init];
    [self.view addSubview:self.profileViewController.view];
}

-(void)hideProfileView:(NSNotification *)notification {
    [self.profileViewController.view removeFromSuperview];
    self.profileViewController= nil;
}

-(void)showChatView:(NSNotification *)notification {
    self.chatViewController= [[ChatViewController alloc] init];
    self.chatViewController.other= [notification.userInfo objectForKey:@"user"];
    [self.view addSubview:self.chatViewController.view];
}

-(void)hideChatView:(NSNotification *)notification {
    [self.chatViewController.view removeFromSuperview];
    self.chatViewController= nil;
}

-(NSFetchedResultsController *)usersFetchedResultsController {
    if(usersFetchedResultsController) { return usersFetchedResultsController; }
    NSManagedObjectContext *context= [[self appDelegate].database managedObjectContext];
    NSEntityDescription *entity= [NSEntityDescription entityForName:@"User" inManagedObjectContext:context];
    NSPredicate *predicate= [NSPredicate predicateWithFormat:@"isHidden== %@ && user!= %@", [NSNumber numberWithBool:NO], [self appDelegate].profile.user];
    NSSortDescriptor *sort= [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
    NSArray *sortDescriptors= [NSArray arrayWithObjects:sort, nil];
    NSFetchRequest *request= [[NSFetchRequest alloc] init];
    request.entity= entity; request.predicate= predicate; request.sortDescriptors= sortDescriptors;
    usersFetchedResultsController= [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    usersFetchedResultsController.delegate= self; [usersFetchedResultsController performFetch:nil];
    return usersFetchedResultsController;
}

-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [mainTableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return sectionIndex== 0 ? 1 : usersFetchedResultsController.fetchedObjects.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section== 0) {
        static NSString *TitleCellIdentifier= @"TitleCellIdentifier";
        TitleCell *titleCell= [tableView dequeueReusableCellWithIdentifier:TitleCellIdentifier];
        if(titleCell== nil) { titleCell= [[TitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TitleCellIdentifier]; }
        titleCell.label.text= @"Users nearby";
        return titleCell;
    }
    
    static NSString *UserCellIdentifier= @"UserCellIdentifier";
    UserCell *userCell= [tableView dequeueReusableCellWithIdentifier:UserCellIdentifier];
    if(userCell== nil) { userCell= [[UserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:UserCellIdentifier]; }
    
    User *user= [usersFetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
    userCell.name.text= (user.name== nil || [user.name isEqualToString: @""]) ? @"Anonymous" : user.name;
    userCell.status.text= (user.status== nil || [user.status isEqualToString:@""]) ? @"No status" : user.status;
    NSDateFormatter *dateFormatter= [[self appDelegate] dateFormatter];
    userCell.last.text= [user.timestamp isEqualToNumber:[NSNumber numberWithInt:0]] ? @"Online" :
        [NSString stringWithFormat:@"Last seen: %@", [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:[user.timestamp longLongValue]/1000]]];
    userCell.unread.frame= [user.gotUnread isEqualToNumber:[NSNumber numberWithBool:YES]] ? CGRectMake(width - 40.0, 10.0, 20.0, 20.0) : CGRectZero;
    return userCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dictUser= [[NSDictionary alloc] initWithObjectsAndKeys:[usersFetchedResultsController.fetchedObjects objectAtIndex:indexPath.row], @"user", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showChatView" object:nil userInfo:dictUser];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section== 0 ? 44.0 : 70.0;
}

-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
-(UIStatusBarStyle)preferredStatusBarStyle { return UIStatusBarStyleLightContent; }
-(void)viewDidLoad { [super viewDidLoad]; }
-(void)didReceiveMemoryWarning { [super didReceiveMemoryWarning]; }

@end