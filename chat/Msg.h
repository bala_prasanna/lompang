//
//  Msg.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Msg: NSManagedObject

@property (nonatomic, retain) NSString *from;
@property (nonatomic, retain) NSString *msg;
@property (nonatomic, retain) NSString *pid;
@property (nonatomic, retain) NSNumber *timestamp;
@property (nonatomic, retain) NSString *to;

@end