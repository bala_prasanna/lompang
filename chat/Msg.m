//
//  Msg.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "Msg.h"


@implementation Msg

@dynamic from;
@dynamic msg;
@dynamic pid;
@dynamic timestamp;
@dynamic to;

@end