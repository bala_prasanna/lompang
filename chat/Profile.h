//
//  Profile.h
//  chat
//
//  Created by Zhongcai Ng on 14/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Profile: NSManagedObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *user;

@end