//
//  Profile.m
//  chat
//
//  Created by Zhongcai Ng on 14/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "Profile.h"

@implementation Profile

@dynamic name;
@dynamic status;
@dynamic user;

@end