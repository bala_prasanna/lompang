//
//  ProfileViewController.h
//  chat
//
//  Created by Zhongcai Ng on 14/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TextfieldCell.h"

@class TextfieldCell;

@interface ProfileViewController: UIViewController <UITableViewDataSource, UITableViewDelegate> {
    CGFloat width;
    CGFloat height;
    CGFloat thick;
    UITableView *profileTableView;
    TextfieldCell *nameCell;
    TextfieldCell *statusCell;
}

@end