//
//  ProfileViewController.m
//  chat
//
//  Created by Zhongcai Ng on 14/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController()
@end

@implementation ProfileViewController

-(id)init {
    self= [super init];
    if(self) {
    }
    return self;
}

-(void)loadView {
    width= [self appDelegate].width;
    height= [self appDelegate].height;
    thick= [self appDelegate].thick;
    self.view= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, height)];
    self.view.backgroundColor= [self appDelegate].beige;
    
    UIView *status= [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, width, 20.0)];
    status.backgroundColor= [UIColor blackColor];
    [self.view addSubview:status];
    
    UIView *top= [[UIView alloc] initWithFrame:CGRectMake(0.0, 20.0, width, 50.0)];
    top.backgroundColor= [self appDelegate].red;
    [self.view addSubview:top];
    
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(0.0, 26.0, width, 36.0)];
    title.font= [UIFont fontWithName:@"ChalkboardSE-Bold" size:22.0];
    title.backgroundColor= [UIColor clearColor];
    title.textAlignment= NSTextAlignmentCenter;
    title.textColor= [self appDelegate].beige;
    title.text= @"Profile";
    [self.view addSubview:title];
    
    UIButton *backBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn addTarget:self action:@selector(fireBack) forControlEvents:UIControlEventTouchUpInside];
    backBtn.frame= CGRectMake(0.0, 20.0, 50.0, 50.0);
    [backBtn setImage:[UIImage imageNamed:@"Back"] forState:UIControlStateNormal];
    [self.view addSubview:backBtn];
    
    profileTableView= [[UITableView alloc] initWithFrame:CGRectMake(0.0, 70.0, width, height -70.0) style:UITableViewStylePlain];
    profileTableView.backgroundColor= [self appDelegate].beige;
    profileTableView.delegate= self;
    profileTableView.dataSource= self;
    profileTableView.separatorStyle= UITableViewCellSeparatorStyleNone;
    [self.view addSubview:profileTableView];
}

-(void)fireBack {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideProfileView" object:nil];
    Profile *profile= [[self appDelegate].database fetchProfile];
    if([profile.name isEqualToString:nameCell.textField.text] && [profile.status isEqualToString:statusCell.textField.text]) { return; }
    NSMutableDictionary *dict= [[NSMutableDictionary alloc] init];
    if(nameCell.textField.text) { [dict setObject:nameCell.textField.text forKey:@"name"]; }
    if(statusCell.textField.text) { [dict setObject:statusCell.textField.text forKey:@"status"]; }
    [[self appDelegate].database updateProfile:dict];
    [[self appDelegate].lompang update:dict];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row== 0) {
        if(nameCell== nil) { nameCell= [[TextfieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]; }
        nameCell.label.text= @"Nickname"; nameCell.textField.placeholder= @"Optional"; nameCell.textField.text= [self appDelegate].profile.name;
        return nameCell;
    }
    
    if(statusCell== nil) { statusCell= [[TextfieldCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil]; }
    statusCell.label.text= @"Status"; statusCell.textField.placeholder= @"Optional"; statusCell.textField.text= [self appDelegate].profile.status;
    return statusCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.row== 0) { [nameCell showTextFieldKeyboard]; [statusCell hideTextFieldKeyboard]; }
    else { [nameCell hideTextFieldKeyboard]; [statusCell showTextFieldKeyboard]; }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate { [nameCell hideTextFieldKeyboard]; [statusCell hideTextFieldKeyboard]; }
-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }
-(void)viewDidLoad { [super viewDidLoad]; }
-(void)didReceiveMemoryWarning { [super didReceiveMemoryWarning]; }

@end