//
//  TextfieldCell.h
//  chat
//
//  Created by Zhongcai Ng on 14/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TextfieldCell: UITableViewCell <UITextFieldDelegate> {
    CGFloat width;
    CGFloat thick;
}

@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UITextField *textField;

-(void)showTextFieldKeyboard;
-(void)hideTextFieldKeyboard;

@end