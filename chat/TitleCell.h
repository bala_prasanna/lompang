//
//  TitleCell.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TitleCell: UITableViewCell {
    CGFloat width;
    CGFloat thick;
}

@property (strong, nonatomic) UILabel *label;

@end