//
//  TitleCell.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "TitleCell.h"

@implementation TitleCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        width= [[UIScreen mainScreen] applicationFrame].size.width;
        thick= [[UIScreen mainScreen] scale]== 2.0 ? 0.5 : 1.0;
        self.selectionStyle= UITableViewCellSelectionStyleNone;
        self.backgroundColor= [self appDelegate].beige;
        
        self.label= [[UILabel alloc] initWithFrame:CGRectMake(5.0, 20.0, width- 10.0, 20.0)];
        self.label.backgroundColor= [UIColor clearColor];
        self.label.font= [UIFont fontWithName:@"AvenirNextCondensed-Bold" size:14.0];
        self.label.textColor= [UIColor grayColor];
        [self.contentView addSubview:self.label];
    }
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated { [super setSelected:selected animated:animated]; }
-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }

@end