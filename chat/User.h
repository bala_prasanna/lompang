//
//  User.h
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface User: NSManagedObject

@property (nonatomic, retain) NSNumber *gotUnread;
@property (nonatomic, retain) NSNumber *isHidden;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSNumber *timestamp;
@property (nonatomic, retain) NSString *user;

@end