//
//  User.m
//  chat
//
//  Created by Zhongcai Ng on 13/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic gotUnread;
@dynamic isHidden;
@dynamic name;
@dynamic status;
@dynamic timestamp;
@dynamic user;

@end