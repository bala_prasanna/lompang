//
//  UserCell.h
//  chat
//
//  Created by Zhongcai Ng on 15/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"

@interface UserCell: UITableViewCell {
    CGFloat width;
    CGFloat thick;
}

@property (strong, nonatomic) UILabel *name;
@property (strong, nonatomic) UILabel *status;
@property (strong, nonatomic) UILabel *last;
@property (strong, nonatomic) UILabel *unread;

@end