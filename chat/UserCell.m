//
//  UserCell.m
//  chat
//
//  Created by Zhongcai Ng on 15/9/14.
//  Copyright (c) 2014 lompang. All rights reserved.
//

#import "UserCell.h"

@implementation UserCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self= [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self) {
        width= [[UIScreen mainScreen] applicationFrame].size.width;
        thick= [[UIScreen mainScreen] scale]== 2.0 ? 0.5 : 1.0;
        self.backgroundView= [[UIView alloc] initWithFrame:self.bounds];
        self.backgroundView.backgroundColor= [self appDelegate].beige;
        self.selectedBackgroundView= [[UIView alloc] initWithFrame:self.bounds];
        self.selectedBackgroundView.backgroundColor= [UIColor clearColor];
        
        UIView *wrapper= [[UIView alloc] initWithFrame:CGRectMake(5.0, 5.0, width- 10.0, 65.0)];
        wrapper.layer.cornerRadius= 4.0;
        wrapper.backgroundColor= [UIColor whiteColor];
        wrapper.layer.borderColor= [UIColor grayColor].CGColor;
        wrapper.layer.borderWidth= thick;
        [self.contentView addSubview:wrapper];
        
        self.name= [[UILabel alloc] initWithFrame:CGRectMake(5.0, 5.0, width- 10.0, 20.0)];
        self.name.backgroundColor= [UIColor clearColor];
        self.name.font= [UIFont boldSystemFontOfSize:19.0];
        self.name.textColor= [UIColor grayColor];
        self.name.textAlignment= NSTextAlignmentCenter;
        [wrapper addSubview:self.name];
        
        self.status= [[UILabel alloc] initWithFrame:CGRectMake(5.0, 25.0, width- 10.0, 20.0)];
        self.status.backgroundColor= [UIColor clearColor];
        self.status.font= [UIFont italicSystemFontOfSize:16.0];
        self.status.textColor= [UIColor grayColor];
        self.status.textAlignment= NSTextAlignmentCenter;
        [wrapper addSubview:self.status];
        
        self.last= [[UILabel alloc] initWithFrame:CGRectMake(5.0, 45.0, width- 10.0, 15.0)];
        self.last.backgroundColor= [UIColor clearColor];
        self.last.font= [UIFont fontWithName:@"Avenir-LightOblique" size:10.0];
        self.last.textColor= [UIColor grayColor];
        self.last.textAlignment= NSTextAlignmentCenter;
        [wrapper addSubview:self.last];
        
        self.unread= [[UILabel alloc] init];
        self.unread.backgroundColor= [UIColor redColor];
        self.unread.textAlignment= NSTextAlignmentCenter;
        self.unread.font= [UIFont systemFontOfSize:14.0];
        self.unread.textColor= [UIColor whiteColor];
        self.unread.layer.cornerRadius= 10.0;
        [self.unread.layer setMasksToBounds:YES];
        self.unread.text= @"1";
        [wrapper addSubview:self.unread];
    }
    return self;
}

-(void)setSelected:(BOOL)selected animated:(BOOL)animated { [super setSelected:selected animated:animated]; }
-(AppDelegate *)appDelegate { return (AppDelegate *)[[UIApplication sharedApplication] delegate]; }

@end